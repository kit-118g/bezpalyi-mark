﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Bezpalyi07
{
    class SimpleLINQ
    {
        public void OutputGroups(LinkedList<Student> students)
        {
            var names =
                from Student s in students
                select s.FirstName;

            var groups =
                from Student s in students
                select s.GroupIndex;

            Console.WriteLine("Groups:");
            foreach (var ng in names.Zip(groups, Tuple.Create))
            {
                Console.WriteLine("|{0,10}|{1,3}|", ng.Item1, ng.Item2);
            }
        }

        public void OutputCourses(LinkedList<Student> students)
        {
            var now = DateTime.Now;

            var names =
                from Student s in students
                select s.FirstName;

            var admissions =
                from Student s in students
                select now.Year - s.AdmissionDate.Year;

            Console.WriteLine("By admissions:");
            foreach (var nc in names.Zip(admissions, Tuple.Create))
            {
                if (now.Month >= 9 && now.Month <= 12)
                {
                    Console.WriteLine("|{0,10}|{1,3}|{2,3}|", nc.Item1, nc.Item2 + 1, 1);
                }
                else
                {
                    Console.WriteLine("|{0,10}|{1,3}|{2,3}|", nc.Item1, nc.Item2, 2);
                }
            }
        }

        public void OutputAge(LinkedList<Student> students)
        {
            var now = DateTime.Now;

            var names =
                from Student s in students
                select s.FirstName;
            var years =
                from Student s in students
                select now.Year - s.Birthday.Year;

            Console.WriteLine("By age:");
            foreach (var ny in names.Zip(years, Tuple.Create))
            {
                Console.WriteLine("|{0,10}|{1,10}|", ny.Item1, ny.Item2);
            }

        }
    }
}
