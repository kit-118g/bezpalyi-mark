﻿using System;
namespace Bezpalyi07
{
    public class Student
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime AdmissionDate { get; set; }
        public string GroupIndex { get; set; }
        public string Faculty { get; set; }
        public string Specialty { get; set; }
        public int AcademicPerformance { get; set; }

        public override string ToString()
        {
            return "First name: " + FirstName +
                "\nSecond name: " + SecondName +
                "\nLast name: " + LastName +
                "\nBirthday: " + Birthday.ToString() +
                "\nAdmission Date: " + AdmissionDate +
                "\nGroup Index: " + GroupIndex +
                "\nFaculty: " + Faculty +
                "\nSpecialty: " + Specialty +
                "\nAcademic Performance: " + AcademicPerformance;
        }
    }
}
