﻿namespace Bezpalyi07
{
    public interface IReader
    {
        string ReadLine();

        string[] ReadWholeFile();

        void Reset();
    }
}
