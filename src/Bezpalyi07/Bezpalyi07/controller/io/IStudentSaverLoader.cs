﻿namespace Bezpalyi07.io
{
    public interface IStudentSaverLoader
    {
        void WriteStudent(int index, bool append);

        void WriteAllStudents();

        Student[] ReadStudents();
    }
}
