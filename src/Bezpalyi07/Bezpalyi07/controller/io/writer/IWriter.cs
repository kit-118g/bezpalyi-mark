﻿namespace Bezpalyi07
{
    public interface IWriter
    {
        bool Write(string content, bool append);
    }
}
