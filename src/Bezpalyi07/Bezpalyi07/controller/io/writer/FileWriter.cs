﻿using System;
using System.IO;

namespace Bezpalyi07
{
    public class FileWriter : IWriter
    {

        private string Path;

        private const int MIN_FILE_PATH_LENGTH = 3;
        
        public FileWriter(string path)
        {
            this.Path = path;
        }

        public bool Write(string content, bool append)
        {
            bool result = true;

            PathValidator.ValidatePath(Path);

            try
            {
                if (append)
                {
                    using (var fileAppender = new StreamWriter(Path, true))
                    {
                        fileAppender.WriteLine(content);
                    }

                } else
                {
                    File.WriteAllText(Path, content);
                }
                
            } catch (IOException e)
            {
                Console.WriteLine("Error while writing the file!");
                Console.WriteLine(e.StackTrace);
                return false;
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine("Error! READ ONLY file!");
                Console.WriteLine(e.StackTrace);
                return false;
            }

            return result;
        }

        public void SetPath(string newPath)
        {
            this.Path = newPath;
        }
    }
}
