﻿using System;
using System.Text;

namespace Bezpalyi07.io
{
    public class FileStudentSaverLoader : IStudentSaverLoader
    {
        private Student[] Students;

        private const int STUDENT_FIELD_COUNT = 9;

        private readonly IWriter Writer;

        private readonly IReader Reader;

        public FileStudentSaverLoader(Student[] students, IWriter writer, IReader reader)
        {
            if(students == null || students.Length < 1)
            {
                throw new ArgumentException("Invalid argument: students");
            }

            Students = students;
            Writer = writer;
            Reader = reader;
        }

        private void CheckIndex(int index)
        {
            if(index < 0 || index >= Students.Length)
            {
                throw new IndexOutOfRangeException("Index " + index + " is out of range");
            }
        }

        public void WriteStudent(int index, bool append)
        {
            CheckIndex(index);
            Writer.Write(Students[index].ToString(), append);
        }

        public void WriteAllStudents()
        {
            StringBuilder formattableString = new StringBuilder();
            foreach(Student student in Students)
            {
                formattableString.Append(student.ToString());
                formattableString.Append('\n');
            }
            Writer.Write(formattableString.ToString(), false);
        }

        public Student[] ReadStudents()
        {
            string[] fileContent = Reader.ReadWholeFile();

            if(fileContent == null || fileContent.Length % STUDENT_FIELD_COUNT != 0)
            {
                throw new ArgumentException("Bad file format");
            }

            int count = fileContent.Length / STUDENT_FIELD_COUNT;
            int counter = 0;

            Student[] readStudents = new Student[count];

            for(int i = 0; i < count; i++)
            {
                try
                {
                    readStudents[i] = new Student
                    {
                        FirstName = fileContent[counter++].Split(':')[1].Remove(0, 1),
                        SecondName = fileContent[counter++].Split(':')[1].Remove(0, 1),
                        LastName = fileContent[counter++].Split(':')[1].Remove(0, 1),
                        Birthday = Convert.ToDateTime(fileContent[counter++].Remove(0, 10).Remove(10).Replace('.', '/')),
                        AdmissionDate = Convert.ToDateTime(fileContent[counter++].Remove(0, 16).Remove(10).Replace('.', '/')),
                        GroupIndex = fileContent[counter++].Split(':')[1].Remove(0, 1),
                        Faculty = fileContent[counter++].Split(':')[1].Remove(0, 1),
                        Specialty = fileContent[counter++].Split(':')[1].Remove(0, 1),
                        AcademicPerformance = int.Parse(fileContent[counter++].Remove(0, 22))
                    };
                } catch (FormatException e)
                {
                    Console.WriteLine("Error while parsing dates!");
                    Console.WriteLine(e.Message);
                }
            }

            return readStudents;
        }
    }
}
