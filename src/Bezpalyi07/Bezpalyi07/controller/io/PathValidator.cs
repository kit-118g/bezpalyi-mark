﻿using System;
using System.IO;

namespace Bezpalyi07.io
{
    public static class PathValidator
    {
        private const int MIN_FILE_PATH_LENGTH = 3;

        public static void ValidatePath(string path)
        {
            if (!File.Exists(path) || path == null || path.Length < MIN_FILE_PATH_LENGTH)
            {
                throw new ArgumentException("Invalid path!");
            }
        }
    }
}
