﻿using System;
using System.Text.RegularExpressions;

namespace Bezpalyi07
{
    class InputController
    {
        private Regex NameRegex = new Regex("[A-Z]+", RegexOptions.IgnoreCase);

        private Regex DateTimeRegex = new Regex("^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$");

        private Regex GroupIndexRegex = new Regex("[A-Z0-9-]+", RegexOptions.IgnoreCase);

        public int GetNextInt(string message)
        {
            Console.Write(message);
            string input = Console.ReadLine();

            if (!int.TryParse(input, out int result))
            {
                Console.WriteLine("Invalid input!");

                return 0;
            }

            return result;
        }
        
        public string GetGroupIndex(string message)
        {
            Console.Write(message);
            string input = Console.ReadLine();

            if(!GroupIndexRegex.IsMatch(input))
            {
                Console.WriteLine("Invalid group index!");
            }

            return input;
        }

        public string GetName(string message)
        {
            Console.Write(message);
            string input = Console.ReadLine();
            if(!NameRegex.IsMatch(input))
            {
                Console.WriteLine("Invalid input name!");
                return "";
            }
            return input;
        }

       public DateTime GetDate(string message)
        {
            Console.Write(message);
            string input = Console.ReadLine();

            if (!DateTimeRegex.IsMatch(input))
            {
                Console.WriteLine("Invalid input date!");

                return new DateTime();
            }

            return Convert.ToDateTime(input);
        }
    }
}
