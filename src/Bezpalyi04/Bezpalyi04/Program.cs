﻿using Bezpalyi01;
using Bezpalyi02;
using System;

namespace Bezpalyi04
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] students = new Student[5] {
                new Student(), new Student(), new Student(), new Student(), new Student()
            };

            students[0].FirstName = "Student 1 FN";
            students[0].SecondName = "Student 1 SN";
            students[0].LastName = "Student 1 LN";
            students[0].Birthday = Convert.ToDateTime("20/10/2000");
            students[0].AdmissionDate = Convert.ToDateTime("20/10/2018");
            students[0].Faculty = "Student 1 Faculty";
            students[0].GroupIndex = "Student 1 Group index";
            students[0].Specialty = "Student 1 Speciality";
            students[0].AcademicPerformance = 10;

            students[1].FirstName = "Student 2 FN";
            students[1].SecondName = "Student 2 SN";
            students[1].LastName = "Student 2 LN";
            students[1].Birthday = Convert.ToDateTime("15/07/2003");
            students[1].AdmissionDate = Convert.ToDateTime("20/10/2017");
            students[1].Faculty = "Student 2 Faculty";
            students[1].GroupIndex = "Student 2 Group index";
            students[1].Specialty = "Student 2 Speciality";
            students[1].AcademicPerformance = 7;

            students[2].FirstName = "Student 3 FN";
            students[2].SecondName = "Student 3 SN";
            students[2].LastName = "Student 3 LN";
            students[2].Birthday = Convert.ToDateTime("01/11/2001");
            students[2].AdmissionDate = Convert.ToDateTime("20/10/2020");
            students[2].Faculty = "Student 3 Faculty";
            students[2].GroupIndex = "Student 3 Group index";
            students[2].Specialty = "Student 3 Speciality";
            students[2].AcademicPerformance = 12;

            students[3].FirstName = "Student 4 FN";
            students[3].SecondName = "Student 4 SN";
            students[3].LastName = "Student 4 LN";
            students[3].Birthday = Convert.ToDateTime("07/12/2002");
            students[3].AdmissionDate = Convert.ToDateTime("20/10/2016");
            students[3].Faculty = "Student 4 Faculty";
            students[3].GroupIndex = "Student 4 Group index";
            students[3].Specialty = "Student 4 Speciality";
            students[3].AcademicPerformance = 9;

            students[4].FirstName = "Student 5 FN";
            students[4].SecondName = "Student 5 SN";
            students[4].LastName = "Student 5 LN";
            students[4].Birthday = Convert.ToDateTime("13/04/1997");
            students[4].AdmissionDate = Convert.ToDateTime("20/10/2019");
            students[4].Faculty = "Student 5 Faculty";
            students[4].GroupIndex = "Student 5 Group index";
            students[4].Specialty = "Student 5 Speciality";
            students[4].AcademicPerformance = 6;

            IMyList<Student> studentList = new SimpleListImpl<Student>();
            studentList.AddAll(students);

            Console.WriteLine("Enter number of student (1 - 5): ");
            string input = Console.ReadLine();

            if (!int.TryParse(input, out int result) || result < 1 || result > 5)
            {
                Console.WriteLine("Invalid input!");
                return;
            }

            Student selected = studentList.Get(result - 1);
            ExtendedStudent extendedStudent = new ExtendedStudent(selected);
            Console.WriteLine("Faculty: " + extendedStudent.ReadOnlyFaculty);
            Console.WriteLine("Full group info: " + extendedStudent.ReadOnlyGroupIndex);
            Console.WriteLine("Current course: " + extendedStudent.GetCurrentCourse());
            Console.WriteLine("Current semester: " + extendedStudent.GetCurrentSemester());
            Console.WriteLine("Age: " + extendedStudent.GetAge());
            Console.ReadKey();
        }
    }
}
