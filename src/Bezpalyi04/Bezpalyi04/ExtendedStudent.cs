﻿using Bezpalyi01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bezpalyi04
{
    class ExtendedStudent : Student
    {
        public string ReadOnlyFaculty { get; }
        public string ReadOnlySpecialty { get; }
        public DateTime ReadOnlyBirthday { get; }
        public DateTime ReadOnlyAdmissionDate { get; }
        public string ReadOnlyGroupIndex { get; }

        public ExtendedStudent(Student student)
        {

            FirstName = student.FirstName;
            SecondName = student.SecondName;
            LastName = student.LastName;
            Faculty = student.Faculty;
            Birthday = student.Birthday;
            AdmissionDate = student.AdmissionDate;
            GroupIndex = student.GroupIndex;
            Specialty = student.Specialty;
            AcademicPerformance = student.AcademicPerformance;

            ReadOnlyFaculty = Faculty;
            ReadOnlySpecialty = Specialty;
            ReadOnlyBirthday = Birthday;
            ReadOnlyGroupIndex = GroupIndex;
            ReadOnlyAdmissionDate = AdmissionDate;
        }

        public int GetCurrentSemester()
        {
            int result = 0;

            int year = AdmissionDate.Year;
            int currentYear = DateTime.Now.Year;
            int currentMonth = DateTime.Now.Month;

            result = (currentYear - year) * 2;

            if (currentMonth < 9)
            {
                result += 2;
            }
            else
            {
                result += 1;
            }

            return result;
        }

        public int GetCurrentCourse()
        {
            return DateTime.Now.Year - AdmissionDate.Year + 1;
        }

        public string GetAge()
        {
            int year = Birthday.Year;
            int month = Birthday.Month;
            int day = DateTime.Now.Day;

            year = DateTime.Now.Year - year;

            if(DateTime.Now.Month < Birthday.Month)
            {
                month = DateTime.Now.Month + Birthday.Month;
            } else if(DateTime.Now.Month == Birthday.Month)
            {
                month = 0;
            } else
            {
                month = DateTime.Now.Month - Birthday.Month;
            }

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(year);
            stringBuilder.Append(" years old; ");
            stringBuilder.Append(month);
            stringBuilder.Append(" monthes; ");
            stringBuilder.Append(day);
            stringBuilder.Append(" days.");

            return stringBuilder.ToString();
        }
    }
}
