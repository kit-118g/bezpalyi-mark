﻿using Bezpalyi01;
using Bezpalyi02;
using System;

namespace Bezpalyi05
{
    delegate bool CompareBy(Student student, string field);
    delegate void Avarage(IMyList<Student> students, string field);

    class SimpleDelegator
    {
        public CompareBy compare { get; set; }
        public Avarage avarage { get; set; }

        public void output(IMyList<Student> students, string field)
        {
            if (compare != null)
            {
                string formatRow = "|{0,10}|{1,10}|{2,10}|{3,10}|{4,10}|{5,2}|{6,5}|{7,5}|{8,3}|";

                foreach (Student s in students)
                {
                    if (compare(s, field))
                    {
                        Console.WriteLine(String.Format(formatRow, 
                            s.FirstName, s.SecondName, s.LastName, s.Birthday, 
                            s.AdmissionDate, s.GroupIndex, s.Faculty, s.Specialty, 
                            s.AcademicPerformance));
                    }
                }
            }
            else
            {
                Console.WriteLine("Choose Deligate before showing");
            }
        }

        public void delete(IMyList<Student> students, string field)
        {
            if (compare != null)
            {
                for (int i = 0; i < students.Size(); i++)
                {
                    if (compare(students.Get(i), field))       // Get starts from 0
                    {
                        students.Remove(i + 1);                 // Remove starts from 1
                        i--;
                    }
                }
            }
            else
            {
                Console.WriteLine("Choose Deligate before deleteing");
            }
        }

        public void avarageAge(IMyList<Student> students, string field)
        {
            if (compare != null)
            {
                float avarage = 0;

                foreach (Student s in students)
                {
                    if (compare(s, field))
                    {
                        avarage += DateTime.Now.Year - s.Birthday.Year;
                    }
                }

                avarage /= students.Size();

                Console.WriteLine("Avarage Age: " + avarage);
            }
            else
            {
                Console.WriteLine("Choose Deligate");
            }
        }

        public void avarageProgress(IMyList<Student> students, string field)
        {
            if (compare != null)
            {
                float avarage = 0;

                foreach (Student s in students)
                {
                    if (compare(s, field))
                    {
                        avarage += s.AcademicPerformance;
                    }
                }

                avarage /= students.Size();

                Console.WriteLine("Avarage Progress: " + avarage);
            }
            else
            {
                Console.WriteLine("Choose Deligate");
            }
        }

        public bool CompareByGroup(Student student, string group)
        {
            return student.GroupIndex.Equals(group);
        }

        public bool CompareByFaculty(Student student, string faculty)
        {
            return student.Faculty.Equals(faculty);
        }

        public bool CompareBySpecialty(Student student, string specialty)
        {
            return student.Specialty.Equals(specialty);
        }
    }
}
