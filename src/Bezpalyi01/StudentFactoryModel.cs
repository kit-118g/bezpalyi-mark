﻿using SharpLab1;

namespace Bezpalyi01
{
    public class StudentFactoryModel
    {
        public Student CreateStudent()
        {
            var controller = new InputController();

            var student = new Student
            {
                FirstName = controller.GetName(Constants.FirstName),
                SecondName = controller.GetName(Constants.SecondName),
                LastName = controller.GetName(Constants.LastName),
                Faculty = controller.GetName(Constants.Faculty),
                Birthday = controller.GetDate(Constants.Birthday),
                AdmissionDate = controller.GetDate(Constants.AdmissionDate),
                GroupIndex = controller.GetGroupIndex(Constants.GroupIndex),
                Specialty = controller.GetName(Constants.Specialty),
                AcademicPerformance = controller.GetNextInt(Constants.AcademicPerformance)
            };

            return student;
        }
    }
}
