﻿using System;

namespace Bezpalyi01
{
    class Program
    {
        private static StudentView studentView = new StudentView();
        private static StudentFactoryModel studentModel = new StudentFactoryModel();

        static void Main(string[] args)
        {
            Console.Write("Enter number of students: ");

            if(!int.TryParse(Console.ReadLine(), out int number) || number < 0)
            {
                Console.WriteLine("Invalid input!");
                return;
            }

            Student[] students = new Student[number];
            for(int i = 0; i < number; i++)
            {
                students[i] = studentModel.CreateStudent();
                Console.WriteLine("Student N" + (i + 1) + " done!\n");
            }
            studentView.PrintStudents(students);
            Console.ReadKey();
        }
    }
}
