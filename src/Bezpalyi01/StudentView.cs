﻿using System;

namespace Bezpalyi01
{
    class StudentView
    {
        public void PrintStudents(Student[] students)
        {
            if(students == null)
            {
                throw new ArgumentNullException(nameof(students));
            }
        
            foreach(var student in students)
            {
                Console.WriteLine();
                Console.WriteLine(student);
            }
        }
    }
}
