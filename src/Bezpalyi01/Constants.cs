﻿namespace SharpLab1
{
    public static class Constants
    {
        public const string FirstName = "Enter first name: ";
        public const string SecondName = "Enter second name: ";
        public const string LastName = "Enter last name: ";
        public const string Birthday = "Enter date of birth in format dd/mm/yyyy: ";
        public const string AdmissionDate = "Enter date of admission in format dd/mm/yyyy: ";
        public const string GroupIndex = "Enter group index (numbers, words, hyphens): ";
        public const string Faculty = "Enter faculty name: ";
        public const string Specialty = "Enter speciality name: ";
        public const string AcademicPerformance = "Enter academic performance: ";
    }
}
