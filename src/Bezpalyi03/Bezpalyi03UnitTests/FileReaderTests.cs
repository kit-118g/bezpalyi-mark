﻿using System;
using Bezpalyi03;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bezpalyi03UnitTests
{
    [TestClass]
    public class FileReaderTests
    {
        private IReader Reader = new FileReader("../../test_files/test_data.txt");

        [TestMethod]
        public void ReadFileLineSuccess()
        {
            string line = Reader.ReadLine();
            Assert.AreEqual("Line 1", line);

            line = Reader.ReadLine();
            Assert.AreEqual("Line 2", line);

            line = Reader.ReadLine();
            Assert.AreEqual("Line 3", line);

            line = Reader.ReadLine();
            Assert.AreEqual("Line 4", line);
        }

        [TestMethod]
        public void ReadWholeFileSuccess()
        {
            string[] expectedLines =
            {
                "Line 1", "Line 2", "Line 3", "Line 4"
            };
            string[] actualLines = Reader.ReadWholeFile();

            for(int i = 0; i < actualLines.Length; i++)
            {
                Assert.AreEqual(expectedLines[i], actualLines[i]);
            }
        }

        [TestMethod]
        public void ReadFileAndResetSuccess()
        {
            string line = Reader.ReadLine();
            Assert.AreEqual("Line 1", line);

            line = Reader.ReadLine();
            Assert.AreEqual("Line 2", line);

            Reader.Reset();

            line = Reader.ReadLine();
            Assert.AreEqual("Line 1", line);

            line = Reader.ReadLine();
            Assert.AreEqual("Line 2", line);

            line = Reader.ReadLine();
            Assert.AreEqual("Line 3", line);

            line = Reader.ReadLine();
            Assert.AreEqual("Line 4", line);
        }

        [TestMethod]
        public void ReadFutherEndEmptyString()
        {
            for (int i = 0; i < 4; i++)
            {
                Reader.ReadLine();
            }

            Assert.AreEqual("", Reader.ReadLine());
        }
    }
}
