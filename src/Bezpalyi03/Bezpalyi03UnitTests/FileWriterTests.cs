﻿using System;
using Bezpalyi03;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bezpalyi03UnitTests
{
    [TestClass]
    public class FileWriterTests
    {
        private static readonly string Path = "../../test_files/test_write_data.txt";

        private IWriter Writer = new FileWriter(Path);

        private IReader Reader = new FileReader(Path);

        [TestMethod]
        public void WriteToFileRewrite() 
        {
            Writer.Write("Test content", false);
            Assert.AreEqual("Test content", Reader.ReadLine());
            Assert.AreEqual("", Reader.ReadLine());
            Reader.Reset();

            Writer.Write("Test new content\nSecond line", false);
            Assert.AreEqual("Test new content", Reader.ReadLine());
            Assert.AreEqual("Second line", Reader.ReadLine());
            Assert.AreEqual("", Reader.ReadLine());
        }

        [TestMethod]
        public void AppendTextToFileSuccess()
        {
            Writer.Write("Old File content", false);
            Assert.AreEqual("Old File content", Reader.ReadLine());
            Assert.AreEqual("", Reader.ReadLine());
            Reader.Reset();

            Writer.Write("\nSome new content", true);
            Assert.AreEqual("Old File content", Reader.ReadLine());
            Assert.AreEqual("Some new content", Reader.ReadLine());
            Assert.AreEqual("", Reader.ReadLine());
            Reader.Reset();
        }
    }
}
