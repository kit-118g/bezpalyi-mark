﻿using System;
using Bezpalyi01;
using Bezpalyi03;
using Bezpalyi03.io;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bezpalyi03UnitTests
{
    [TestClass]
    public class FileStudentSaverLoaderTest
    {

        private IStudentSaverLoader SaverLoader;

        private Student[] students = new Student[5] {
                new Student(), new Student(), new Student(), new Student(), new Student()
        };

        [TestInitialize]
        public void SetUpTest()
        {
            students[0].FirstName = "Student 1 FN";
            students[0].SecondName = "Student 1 SN";
            students[0].LastName = "Student 1 LN";
            students[0].Birthday = Convert.ToDateTime("20/10/2000");
            students[0].AdmissionDate = Convert.ToDateTime("20/10/2012");
            students[0].Faculty = "Student 1 Faculty";
            students[0].GroupIndex = "Student 1 Group index";
            students[0].Specialty = "Student 1 Speciality";
            students[0].AcademicPerformance = 10;

            students[1].FirstName = "Student 2 FN";
            students[1].SecondName = "Student 2 SN";
            students[1].LastName = "Student 2 LN";
            students[1].Birthday = Convert.ToDateTime("15/07/2003");
            students[1].AdmissionDate = Convert.ToDateTime("20/10/2015");
            students[1].Faculty = "Student 2 Faculty";
            students[1].GroupIndex = "Student 2 Group index";
            students[1].Specialty = "Student 2 Speciality";
            students[1].AcademicPerformance = 7;

            students[2].FirstName = "Student 3 FN";
            students[2].SecondName = "Student 3 SN";
            students[2].LastName = "Student 3 LN";
            students[2].Birthday = Convert.ToDateTime("01/11/2001");
            students[2].AdmissionDate = Convert.ToDateTime("20/10/2013");
            students[2].Faculty = "Student 3 Faculty";
            students[2].GroupIndex = "Student 3 Group index";
            students[2].Specialty = "Student 3 Speciality";
            students[2].AcademicPerformance = 12;

            students[3].FirstName = "Student 4 FN";
            students[3].SecondName = "Student 4 SN";
            students[3].LastName = "Student 4 LN";
            students[3].Birthday = Convert.ToDateTime("07/02/2002");
            students[3].AdmissionDate = Convert.ToDateTime("20/10/2014");
            students[3].Faculty = "Student 4 Faculty";
            students[3].GroupIndex = "Student 4 Group index";
            students[3].Specialty = "Student 4 Speciality";
            students[3].AcademicPerformance = 9;

            students[4].FirstName = "Student 5 FN";
            students[4].SecondName = "Student 5 SN";
            students[4].LastName = "Student 5 LN";
            students[4].Birthday = Convert.ToDateTime("13/04/1997");
            students[4].AdmissionDate = Convert.ToDateTime("20/10/2009");
            students[4].Faculty = "Student 5 Faculty";
            students[4].GroupIndex = "Student 5 Group index";
            students[4].Specialty = "Student 5 Speciality";
            students[4].AcademicPerformance = 6;

            SaverLoader = new FileStudentSaverLoader(
                students, 
                new FileWriter("../../test_files/test_sl_data.txt"), 
                new FileReader("../../test_files/test_sl_data.txt")
            );
        }

        [TestMethod]
        public void WriteStudentWithoutAppendRewrite()
        {
            SaverLoader.WriteStudent(0, false);
            Student[] actualStudent = SaverLoader.ReadStudents();
            Assert.AreEqual(1, actualStudent.Length);
            Assert.AreEqual(students[0].FirstName, actualStudent[0].FirstName);
            Assert.AreEqual(students[0].SecondName, actualStudent[0].SecondName);
            Assert.AreEqual(students[0].LastName, actualStudent[0].LastName);
            Assert.AreEqual(students[0].Specialty, actualStudent[0].Specialty);
            Assert.AreEqual(students[0].Birthday, actualStudent[0].Birthday);
            Assert.AreEqual(students[0].AdmissionDate, actualStudent[0].AdmissionDate);
            Assert.AreEqual(students[0].AcademicPerformance, actualStudent[0].AcademicPerformance);
            Assert.AreEqual(students[0].Faculty, actualStudent[0].Faculty);
            Assert.AreEqual(students[0].GroupIndex, actualStudent[0].GroupIndex);

            SaverLoader.WriteAllStudents();
            Student[] actualStudents = SaverLoader.ReadStudents();
            Assert.AreEqual(5, actualStudents.Length);

            int i = 0;
            foreach(Student student in students)
            {
                Assert.IsTrue(StudentsAreEqual(student, actualStudents[i++]));
            }
        }
        
        private bool StudentsAreEqual(Student student1, Student student2)
        {
            bool result = true;

            if(student1.FirstName != student2.FirstName ||
                student1.SecondName != student2.SecondName ||
                student1.LastName != student2.LastName ||
                student1.AcademicPerformance != student2.AcademicPerformance ||
                !student1.AdmissionDate.Equals(student2.AdmissionDate) ||
                !student1.Birthday.Equals(student2.Birthday) ||
                student1.Faculty != student2.Faculty ||
                student1.GroupIndex != student2.GroupIndex ||
                student1.Specialty != student2.Specialty)
            {
                result = false;
            }

            return result;
        }
    }
}
