﻿namespace Bezpalyi03
{
    public interface IWriter
    {
        bool Write(string content, bool append);
    }
}
