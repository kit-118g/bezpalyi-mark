﻿using Bezpalyi01;

namespace Bezpalyi03.io
{
    public interface IStudentSaverLoader
    {
        void WriteStudent(int index, bool append);

        void WriteAllStudents();

        Student[] ReadStudents();
    }
}
