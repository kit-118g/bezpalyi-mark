﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bezpalyi03
{
    public interface IReader
    {
        string ReadLine();

        string[] ReadWholeFile();

        void Reset();
    }
}
