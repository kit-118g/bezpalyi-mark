﻿using Bezpalyi03.io;
using System;
using System.IO;

namespace Bezpalyi03
{
    public class FileReader : IReader
    {

        private string Path;

        private string[] Content;

        private int LineIndex;

        public FileReader(string path)
        {
            Path = path;
            LineIndex = 0;
            Content = null;
        }

        public string ReadLine()
        {
            PathValidator.ValidatePath(Path);

            if (Content == null)
            {
                Content = File.ReadAllLines(Path);
                return Content[LineIndex++];
            }

            if(LineIndex >= Content.Length)
            {
                Console.WriteLine("You at the end of file!");
                return "";
            }

            return Content[LineIndex++];
        }

        public string[] ReadWholeFile()
        {
            PathValidator.ValidatePath(Path);
            Content = File.ReadAllLines(Path);
            return Content;
        }

        public void SetPath(string newPath)
        {
            this.Path = newPath;
        }

        public void Reset()
        {
            Content = null;
            LineIndex = 0;
        }
    }
}
