﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bezpalyi01;
using Bezpalyi02;

namespace Bezpalyi02UnitTests
{
    [TestClass]
    public class SimpleListImplTests
    {
        private readonly Student[] students = new Student[5] {
                new Student(), new Student(), new Student(), new Student(), new Student()
            };

        private IMyList<Student> studentList;

        public SimpleListImplTests()
        {
            students[0].FirstName = "Student 1 FN";
            students[0].SecondName = "Student 1 SN";
            students[0].LastName = "Student 1 LN";
            students[0].Birthday = Convert.ToDateTime("20/10/2000");
            students[0].AdmissionDate = Convert.ToDateTime("20/10/2012");
            students[0].Faculty = "Student 1 Faculty";
            students[0].GroupIndex = "Student 1 Group index";
            students[0].Specialty = "Student 1 Speciality";
            students[0].AcademicPerformance = 10;

            students[1].FirstName = "Student 2 FN";
            students[1].SecondName = "Student 2 SN";
            students[1].LastName = "Student 2 LN";
            students[1].Birthday = Convert.ToDateTime("15/07/2003");
            students[1].AdmissionDate = Convert.ToDateTime("20/10/2015");
            students[1].Faculty = "Student 2 Faculty";
            students[1].GroupIndex = "Student 2 Group index";
            students[1].Specialty = "Student 2 Speciality";
            students[1].AcademicPerformance = 7;

            students[2].FirstName = "Student 3 FN";
            students[2].SecondName = "Student 3 SN";
            students[2].LastName = "Student 3 LN";
            students[2].Birthday = Convert.ToDateTime("01/11/2001");
            students[2].AdmissionDate = Convert.ToDateTime("20/10/2013");
            students[2].Faculty = "Student 3 Faculty";
            students[2].GroupIndex = "Student 3 Group index";
            students[2].Specialty = "Student 3 Speciality";
            students[2].AcademicPerformance = 12;

            students[3].FirstName = "Student 4 FN";
            students[3].SecondName = "Student 4 SN";
            students[3].LastName = "Student 4 LN";
            students[3].Birthday = Convert.ToDateTime("07/02/2002");
            students[3].AdmissionDate = Convert.ToDateTime("20/10/2014");
            students[3].Faculty = "Student 4 Faculty";
            students[3].GroupIndex = "Student 4 Group index";
            students[3].Specialty = "Student 4 Speciality";
            students[3].AcademicPerformance = 9;

            students[4].FirstName = "Student 5 FN";
            students[4].SecondName = "Student 5 SN";
            students[4].LastName = "Student 5 LN";
            students[4].Birthday = Convert.ToDateTime("13/04/1997");
            students[4].AdmissionDate = Convert.ToDateTime("20/10/2009");
            students[4].Faculty = "Student 5 Faculty";
            students[4].GroupIndex = "Student 5 Group index";
            students[4].Specialty = "Student 5 Speciality";
            students[4].AcademicPerformance = 6;
        }

        [TestInitialize]
        public void TestInitialize()
        {
            studentList = new SimpleListImpl<Student>();
        }

        //methodname_case_result

        [TestMethod]
        public void Add_CheckSize_SavedItems()
        {
            studentList.Add((students[0]));
            Assert.AreEqual(1, studentList.Size());

            studentList.Add((students[1]));
            Assert.AreEqual(2, studentList.Size());

            studentList.Add((students[2]));
            Assert.AreEqual(3, studentList.Size());

            studentList.Add((students[3]));
            Assert.AreEqual(4, studentList.Size());

            studentList.Add((students[4]));
            Assert.AreEqual(5, studentList.Size());

            Assert.AreEqual(students[0], studentList.Get(0));
            Assert.AreEqual(students[1], studentList.Get(1));
            Assert.AreEqual(students[2], studentList.Get(2));
            Assert.AreEqual(students[3], studentList.Get(3));
            Assert.AreEqual(students[4], studentList.Get(4));
        }

        [TestMethod]
        public void Add_ByIndex_SavedItems()
        {
            studentList.Add(students[0]);
            studentList.Add(students[1]);
            studentList.Add(students[2]);

            studentList.Add(students[3], 1);

            Assert.AreEqual(students[0], studentList.Get(0));
            Assert.AreEqual(students[3], studentList.Get(1));
            Assert.AreEqual(students[1], studentList.Get(2));
            Assert.AreEqual(students[2], studentList.Get(3));

            Assert.AreEqual(4, studentList.Size());
        }

        [TestMethod]
        public void Add_All_SavedItems()
        {
            studentList.AddAll(students);

            Assert.AreEqual(5, studentList.Size());

            Assert.AreEqual(students[0], studentList.Get(0));
            Assert.AreEqual(students[1], studentList.Get(1));
            Assert.AreEqual(students[2], studentList.Get(2));
            Assert.AreEqual(students[3], studentList.Get(3));
            Assert.AreEqual(students[4], studentList.Get(4));
        }

        [TestMethod]
        public void Remove_ChangeSize_DeletedItem()
        {
            studentList.AddAll(students);
            Assert.AreEqual(5, studentList.Size());

            Assert.AreEqual(students[0], studentList.Remove(students[0]));
            Assert.AreEqual(4, studentList.Size());
            Assert.AreNotEqual(students[0], studentList.Get(0));

            Assert.AreEqual(students[4], studentList.Remove(3));
            Assert.AreEqual(3, studentList.Size());
            Assert.AreNotEqual(students[4], studentList.Get(2));
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentOutOfRangeException))]
        public void AddAll_IndexOutOfBound_Exception()
        {
            studentList.AddAll(students);
            studentList.Get(6);
        }

        [TestMethod]
        public void Clear_AllContainer_RemoveAllItems()
        {
            studentList.AddAll(students);
            Assert.AreEqual(5, studentList.Size());

            studentList.Clear();
            Assert.AreEqual(0, studentList.Size());
        }

        [TestMethod]
        public void Contains_InContainer_Exists()
        {
            studentList.Add(students[0]);
            studentList.Add(students[1]);
            studentList.Add(students[2]);

            Assert.IsTrue(studentList.Contains(students[0]));
            Assert.IsTrue(studentList.Contains(students[1]));
            Assert.IsTrue(studentList.Contains(students[2]));
            Assert.IsFalse(studentList.Contains(students[3]));
            Assert.IsFalse(studentList.Contains(students[4]));
        }

        [TestMethod]
        public void Replace_Item_ReplacedItem()
        {
            studentList.Add(students[0]);
            studentList.Add(students[1]);
            studentList.Add(students[2]);

            studentList.Replace(students[3], 1);

            Assert.AreEqual(students[0], studentList.Get(0));
            Assert.AreEqual(students[3], studentList.Get(1));
            Assert.AreEqual(students[2], studentList.Get(2));

            Assert.AreEqual(3, studentList.Size());
        }
    }
}
